const weatherApi = {
    key : "8d3237e9f25653e6613278d4d8089820",
    baseurl : "https://api.openweathermap.org/data/2.5/weather"
  }
  
  const searchinput = document.getElementById('search-city');
  
  searchinput.addEventListener('keypress',(event)=>{
    if(event.keyCode==13){
      console.log(searchinput.value);
      getweather(searchinput.value);
    }
  });
  function getweather(city){
     fetch(`${weatherApi.baseurl}?q=${city}&appid=${weatherApi.key}&units=metric`)
     .then(weather=>{
       return weather.json();
     }).then(showweather)
  }
  function showweather(weather){
    console.log(weather);
    let city = document.getElementById('city');
    city.innerText = `${weather.name} , ${weather.sys.country}`
    let temp = document.getElementById('temp');
    temp.innerHTML = `${weather.main.temp}&deg;C`
    let max_min = document.getElementById("min_max");
    if(max_min!==null && max_min){
      max_min.innerHTML = `${Math.floor(weather.main.temp_min)}&deg;C (min) / ${Math.ceil(weather.main.temp_max)}&deg;C (max) `;
    }
    let weatherType = document.getElementById('weather-type');
    weatherType.innerHTML = `${weather.weather[0].main}`;
    let date = document.getElementById('Date');
    let todayDate = new Date();
    date.innerHTML = dateMange(todayDate);
  
    if(weatherType.textContent=='Clouds'){
      document.body.style.backgroundImage="url('https://images.pexels.com/photos/2114014/pexels-photo-2114014.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500')";
    }else if(weatherType.textContent=="Rain"){
      document.body.style.backgroundImage ="url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROTbjsMn3-pScl2pGyc2jZY638Tr9H0uURLw&usqp=CAU')";
    }else if(weatherType.textContent=="Haze"){
      document.body.style.backgroundImage="url('https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/ac93cac7-47e0-4320-b0ad-6257f355000c/d4ptn0u-299581c9-429c-46fe-ba0a-07b963135464.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2FjOTNjYWM3LTQ3ZTAtNDMyMC1iMGFkLTYyNTdmMzU1MDAwY1wvZDRwdG4wdS0yOTk1ODFjOS00MjljLTQ2ZmUtYmEwYS0wN2I5NjMxMzU0NjQuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.z6RBldPE8-ManjoIV-7v0eE3-oz2zP5MPfoQtsyfOLo')";
    }else if(weatherType.textContent=="snow"){
      document.body.style.backgroundImage="url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3DYpZOqDXYjmujJvN1p7wUxaFvKSc_DDZVAbt9MCEVGiOjlAN50mWoSrN-kBu82WD_o4&usqp=CAU')";
    }else if(weatherType.textContent=="Thunderstorm"){
      document.body.style.backgroundImage="url('https://www.konnecthq.com/wp-content/uploads/2021/01/What-Types-of-Clouds-Produce-Thunderstorms-22-1-1-758x635.jpg')";
    }else if(weatherType.textContent=="Smoke"){
      document.body.style.backgroundImage="url('https://ak.picdn.net/shutterstock/videos/2718020/thumb/1.jpg')";
    }else if(weatherType.textContent=="Clear"){
      document.body.style.backgroundImage="url('https://previews.123rf.com/images/speedphoto/speedphoto1410/speedphoto141000104/32432450-clear-cloud-in-the-sky.jpg')";
    }else if(weatherType.textContent=="Mist"){
      document.body.style.backgroundImage="url('https://c0.wallpaperflare.com/preview/1/382/496/nature-weather-fog-outdoors.jpg')";
    }
  
  }
  function dateMange(dateAgr) {
     let days = ["sunday","monday","tuesday","wednesday","friday","saturday"];
    
     let mnts = ["januray","february","March","April","May","June","July","August","September","october","November","December"];
      
     let yr = dateAgr.getFullYear();
     let mnt = mnts[dateAgr.getMonth()];
     let date = dateAgr.getDate();
     let day = days[dateAgr.getDay()];
     return `${date} ${mnt} (${day}) ,${yr}`
    
  }
  